package com.pj109.xkorey.copyio

import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import com.pj109.xkorey.model.PageShowImage
import android.support.v7.widget.GridLayoutManager
import com.bumptech.glide.Glide
import android.support.v7.widget.DefaultItemAnimator
import android.util.TypedValue
import android.view.View
import android.widget.ImageView


class MainActivity : AppCompatActivity() {

    var recyclerView: RecyclerView? =null
    var adapter:HomePageAdapter? = null
    var imageList: MutableList<PageShowImage>? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var toolbar: Toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar);

        initCollapsingToolbar()

        recyclerView = findViewById(R.id.recycler_view)

        imageList = arrayListOf()

        adapter= HomePageAdapter(this, arrayListOf())

        adapter!!.imageList=imageList

        val mLayoutManager = GridLayoutManager(this, 1)

        recyclerView!!.setLayoutManager(mLayoutManager)
        recyclerView!!.addItemDecoration(GridSpacingItemDecoration(1, dpToPx(10), true))
        recyclerView!!.setItemAnimator(DefaultItemAnimator())
        recyclerView!!.setAdapter(adapter)

        prepareData()

        try {
            Glide.with(this).load(R.drawable.cover).into(findViewById<View>(R.id.backdrop) as ImageView)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun initCollapsingToolbar() {
        var collapsingToolbar: CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)
        collapsingToolbar.setTitle(" ")
        var appBarLayout: AppBarLayout = findViewById(R.id.appbar)
        appBarLayout.setExpanded(true)

        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = false
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.title = getString(R.string.app_name)
                    isShow = true
                } else if (isShow) {
                    collapsingToolbar.title = " "
                    isShow = false
                }
            }
        })
    }

    fun prepareData(){
        var i0 = PageShowImage();
        i0.name="http://176.122.176.240:8008/facebook/5642facebookde92a047fa3e7feaf0dd8faf6ed2437510.png"
        i0.groupId=5642
        imageList!!.add(i0)

        var i1 = PageShowImage();
        i1.name="http://176.122.176.240:8008/weibo/856weibo12fa1d0991c2f2e6b93888af98e7e3cd4.png"
        i1.groupId=856
        imageList!!.add(i1)

        var i2 = PageShowImage();
        i2.name="http://176.122.176.240:8008/facebook/5687facebookc26f3d94c6bfa6b8ccf5738b72aa4ff710.png"
        i2.groupId=5687
        imageList!!.add(i2)


        var i3 = PageShowImage();
        i3.name="http://176.122.176.240:8008/facebook/5948facebooke9fbf09248d02b252b5e759fa789fe986.png"
        i3.groupId=5948
        imageList!!.add(i3)


        var i4 = PageShowImage();
        i4.name="http://176.122.176.240:8008/weibo/206weibo6240e9e6dffda96d6122b8881e8fa9e02.png"
        i4.groupId=5948
        imageList!!.add(i4)


        var i5 = PageShowImage();
        i5.name="http://176.122.176.240:8008/weibo/282weiboe1085b301bd6461b500933753cccf5788.png"
        i5.groupId=282
        imageList!!.add(i5)

        var i6 = PageShowImage();
        i6.name="http://176.122.176.240:8008/weibo/1253weiboa2ea48b70b07f2e5441573581019488b10.png"
        i6.groupId=1253
        imageList!!.add(i6)


        var i7 = PageShowImage();
        i7.name="http://176.122.176.240:8008/facebook/5924facebook6b66bc60f2d454f235a8b6a16073f6f213.png"
        i7.groupId=5924
        imageList!!.add(i7)


        var i8 = PageShowImage();
        i8.name="http://176.122.176.240:8008/weibo/1333weiboeb1d4a4fe95c40b5e3ae1ba1327385159.png"
        i8.groupId=1333
        imageList!!.add(i8)


        var i9 = PageShowImage();
        i9.name="http://176.122.176.240:8008/facebook/5759facebook041a8f10e634e3837a8704e80056c68e1.png"
        i9.groupId=5759
        imageList!!.add(i9)

        var i10 = PageShowImage();
        i10.name="http://176.122.176.240:8008/facebook/5651facebook83a6be485da1121303c3957eabd0c7a58.png"
        i10.groupId=5651
        imageList!!.add(i10)

        var i11 = PageShowImage();
        i11.name="http://176.122.176.240:8008/facebook/5905facebook94eec7df5e3393499782cda80b3b4e8710.png"
        i11.groupId=5905
        imageList!!.add(i11)


        var i12 = PageShowImage();
        i12.name="http://176.122.176.240:8008/facebook/5750facebook886033d9ba1a74bf1f167e8686272ac42.png"
        i12.groupId=5905
        imageList!!.add(i12)


        var i13 = PageShowImage();
        i13.name="http://176.122.176.240:8008/facebook/5807facebook8a053eac78ecbc6e7107f6d5a1cbb8f78.png"
        i13.groupId=5807
        imageList!!.add(i13)


        var i14 = PageShowImage();
        i14.name="http://176.122.176.240:8008/facebook/5993facebook96a48989afc83c9671306db4bece5c348.png"
        i14.groupId=5993
        imageList!!.add(i14)


        var i15 = PageShowImage();
        i15.name="http://176.122.176.240:8008/facebook/5877facebook89b460fa68215648dc9a63a08c56bd2515.png"
        i15.groupId=5877
        imageList!!.add(i15)

    }


    inner class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    private fun dpToPx(p:Int):Int{
        val r = resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, p.toFloat(), r.getDisplayMetrics()));
    }


}
