package com.pj109.xkorey.copyio

import android.content.Context
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.pj109.xkorey.model.PageShowImage


class HomePageAdapter : RecyclerView.Adapter<HomePageAdapter.MyViewHolder> {

    var mContext: Context? = null;
    var imageList: MutableList<PageShowImage>? = null;

    class MyViewHolder : RecyclerView.ViewHolder {
        var  title: TextView? = null;
        var  count: TextView? = null;
        var thumbnail: ImageView? =null;
        var overflow:ImageView? =null;
        constructor(itemView: View) : super(itemView) {
            title = itemView.findViewById(R.id.title)
            count = itemView.findViewById(R.id.count)
            thumbnail = itemView.findViewById(R.id.thumbnail) as ImageView
            overflow = itemView.findViewById(R.id.overflow) as ImageView
        }

    }

    constructor(mContext:Context,imageList:MutableList<PageShowImage>){
        this.mContext=mContext;
        this.imageList=imageList;
    }



    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HomePageAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(p0.getContext())
                .inflate(R.layout.home_page, p0, false);
        return MyViewHolder(itemView);
    }

    override fun onBindViewHolder(holder:MyViewHolder,p: Int){
        var image:PageShowImage = imageList!![p];
        holder.title!!.setText("groupId");
        holder.count!!.setText(""+image!!.groupId);
        Glide.with(mContext).load(image.name).into(holder.thumbnail)
        holder.overflow!!.setOnClickListener { holder.overflow?.let{showPopupMenu(it)} }
    }

    private fun showPopupMenu(overflow: View) {
        var popup: PopupMenu = mContext?.let { PopupMenu(it,overflow) }!!
        var inflater: MenuInflater = popup.getMenuInflater()
        inflater.inflate(R.menu.home_page, popup.menu)
        popup.setOnMenuItemClickListener(MyMenuItemClickListener())
        popup.show()
    }

    inner class MyMenuItemClickListener : PopupMenu.OnMenuItemClickListener{
        override fun onMenuItemClick(p0: MenuItem?): Boolean {
            if (p0!!.itemId == R.id.action_add_favourite){
                mContext?.let {Toast.makeText(it, "查看详细", Toast.LENGTH_SHORT).show()};
                return true;
            }
            if(p0!!.itemId == R.id.action_play_next){
                mContext?.let {Toast.makeText(it, "收藏", Toast.LENGTH_SHORT).show()};
                return true;
            }
            return false;
        }

    }

    override fun getItemCount():Int{
        return imageList!!.size;
    }
}